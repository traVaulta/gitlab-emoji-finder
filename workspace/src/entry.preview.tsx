import { createQwikCity } from '@builder.io/qwik-city/middleware/node';
// eslint-disable-next-line import/no-unresolved
import qwikCityPlan from '@qwik-city-plan';

import render from './entry.ssr';

export default createQwikCity({ render, qwikCityPlan });
