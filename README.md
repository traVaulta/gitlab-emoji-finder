# Qwik GitLab Emoji Finder

Helps you find the right emoji for GitLab qwik-ly.
> _Project built using Qwik - The JavaScript Framework With O(1) Load Time._

Preview:

- go to [deployed page](https://qwik-gitlab-emoji-finder.netlify.app/)
- here is a preview on local machine in development

![Screenshot of application window](./workspace/docs/screenshot.png "Kanban board")

Navigate to `workspace` directory to play around with the project, you will find more instructions in a README.md file there.


## Author

- [Matija Čvrk](https://www.linkedin.com/in/consultant-matija-cvrk-1388b3101/)
