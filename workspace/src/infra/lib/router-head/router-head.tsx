import { component$ } from '@builder.io/qwik';
import { useDocumentHead, useLocation } from '@builder.io/qwik-city';

export const RouterHead = component$(() => {
  const head = useDocumentHead();
  const loc = useLocation();

  return (
    <>
      <title>Qwik GitLab Emoji Finder: {head.title}</title>

      <link rel="canonical" href={loc.href}/>
      <meta name="viewport" content="width=device-width, initial-scale=1.0"/>

      <link rel="icon" type="image/png" href="https://about.gitlab.com/ico/favicon-16x16.png"/>

      {head.meta.map((m: any) => (<meta {...m} />))}

      {head.links.map((l: any) => (<link {...l} />))}

      {head.styles.map((s) => (
        <style {...s.props} dangerouslySetInnerHTML={s.style}/>
      ))}
    </>
  );
});
