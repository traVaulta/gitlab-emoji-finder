module.exports = {
  root: true,
  env: {
    browser: true,
    es2021: true,
    node: true,
  },
  extends: [
    'eslint:recommended',
    'plugin:@typescript-eslint/recommended',
    'plugin:qwik/recommended',
    'plugin:import/typescript',
    'plugin:import/recommended'
  ],
  parser: '@typescript-eslint/parser',
  parserOptions: {
    tsconfigRootDir: __dirname,
    project: ['./tsconfig.json'],
    ecmaVersion: 2021,
    sourceType: 'module',
    ecmaFeatures: {
      jsx: true,
    },
  },
  plugins: ['@typescript-eslint'],
  settings: {
    "import/resolver": {
      typescript: {}
    }
  },
  rules: {
    "eol-last": "error",
    "guard-for-in": "error",
    "max-len": [
      "error",
      {
        "code": 80
      }
    ],
    "max-lines": [
      "error",
      {
        "max": 90,
        "skipComments": true,
        "skipBlankLines": true
      }
    ],
    'no-console': [
      'error',
      {
        allow: [
          'warn',
          'error'
        ]
      }
    ],
    "no-multiple-empty-lines": [
      "error",
      {
        "max": 1,
        "maxBOF": 0,
        "maxEOF": 1
      }
    ],
    '@typescript-eslint/no-explicit-any': 'off',
    '@typescript-eslint/explicit-module-boundary-types': 'off',
    '@typescript-eslint/no-inferrable-types': 'off',
    '@typescript-eslint/no-non-null-assertion': 'off',
    '@typescript-eslint/no-empty-interface': 'off',
    '@typescript-eslint/no-namespace': 'off',
    '@typescript-eslint/no-empty-function': 'off',
    '@typescript-eslint/no-this-alias': 'off',
    '@typescript-eslint/ban-types': 'off',
    '@typescript-eslint/ban-ts-comment': 'off',
    "@typescript-eslint/no-unused-vars": [
      "error",
      {
        "argsIgnorePattern": "^_",
        "varsIgnorePattern": "^_"
      }
    ],
    "@typescript-eslint/prefer-for-of": "error",
    "@typescript-eslint/quotes": [
      "error",
      "single"
    ],
    "@typescript-eslint/consistent-type-assertions": [
      "error",
      {
        "assertionStyle": "as"
      }
    ],
    "@typescript-eslint/semi": "error",
    "@typescript-eslint/no-extra-semi": "error",
    "@typescript-eslint/comma-dangle": "error",
    "@typescript-eslint/member-ordering": "error",
    "@typescript-eslint/naming-convention": [
      "error",
      {
        "selector": [
          "function",
          "classMethod"
        ],
        "format": [
          "camelCase"
        ]
      }
    ],
    "@typescript-eslint/object-curly-spacing": [
      1,
      "always"
    ],
    "@typescript-eslint/space-before-blocks": "error",
    "@typescript-eslint/brace-style": "error",
    "@typescript-eslint/type-annotation-spacing": [
      "error",
      {
        "before": false,
        "after": true,
        "overrides": {
          "arrow": {
            "before": true,
            "after": true
          }
        }
      }
    ],
    'import/order': [
      'error',
      {
        'newlines-between': 'always',
        'groups': [
          ['builtin', 'external'],
          ['internal', 'parent', 'sibling', 'index'],
          'object'
        ],
        'alphabetize': {
          'order': 'asc',
          'caseInsensitive': false
        },
        'warnOnUnassignedImports': true
      }
    ]
  },
};
