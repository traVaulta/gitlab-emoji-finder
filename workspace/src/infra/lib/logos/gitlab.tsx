import { component$ } from '@builder.io/qwik';

export default component$(() => {
  return (
    <logo style={{ 'text-align': 'center' }}>
      <a href="https://about.gitlab.com">
        <img
          alt="GitLab Logo"
          width={400}
          height={320}
          src="https://about.gitlab.com/images/press/logo/svg/gitlab-logo-500.svg"
        />
      </a>
    </logo>
  );
});
