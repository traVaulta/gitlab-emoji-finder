import { renderToStream, RenderToStreamOptions } from '@builder.io/qwik/server';
// eslint-disable-next-line import/no-unresolved
import { manifest } from '@qwik-client-manifest';

import Root from './root';

export default function (opts: RenderToStreamOptions) {
  const containerAttributes = {
    lang: 'en-us',
    ...opts.containerAttributes
  };
  return renderToStream(<Root/>, { manifest, ...opts, containerAttributes });
}
