import { component$, useContext } from '@builder.io/qwik';

import { GroupType } from '../type/model';
import { EmojiGroupContext } from '~/core/domain/group/context';

export interface GroupPickerProps {
  group: GroupType;
}

export default component$(({ group }: GroupPickerProps) => {
  const state = useContext(EmojiGroupContext);
  const groupType = `${group}`;
  const style = [
    'h-12',
    'w-36',
    'm-4',
    'outline-0',
    'border-0',
    'cursor-pointer',
    'capitalize',
    'text-white',
    'bg-teal-600'
  ].join(' ');
  return (
    <button
      type="button"
      class={ style }
      onClick$={() => state.filter = groupType}
    >
      {group}
    </button>
  );
});
