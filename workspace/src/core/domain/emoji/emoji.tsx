import { $, component$, useOn } from '@builder.io/qwik';

import { Emoji } from '~/core/domain/emoji/model';

export interface EmojiProps {
  emoji: Emoji;
}

export const permission = {
  name: 'clipboard-write' as PermissionName
} as PermissionDescriptor;

export const toClipboard = (content: string) => async () => {
  const result = await navigator.permissions.query(permission);
  if (result.state === 'granted' || result.state === 'prompt') {
    await navigator.clipboard.writeText(content);
  }
};

export default component$(({ emoji }: EmojiProps) => {
  const { content, description } = emoji;
  useOn('click', $(() => toClipboard(content)()));
  return (
    <div class="w-16 h-16 cursor-pointer" title={description}>
      {content}
    </div>
  );
});
